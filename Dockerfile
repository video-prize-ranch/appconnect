FROM --platform=$BUILDPLATFORM golang:alpine AS build

ARG TARGETARCH

WORKDIR /src
RUN apk --no-cache add git
COPY . .

RUN go mod download
RUN GOOS=linux GOARCH=$TARGETARCH CGO_ENABLED=0 go build

FROM scratch as bin

WORKDIR /app
COPY --from=build /src/appconnect .

CMD ["/app/appconnect"]
