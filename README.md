# appconnect
Lightweight app connection filtering and healthcheck responder. Designed specifically to be used for running bcow.xyz services.

## Usage
```
./appconnect -l :5000 -r 8 -b example1.com>127.0.0.1:3000 -b example2.com>127.0.0.1:3001
```

### Flags
* `-l` - Listen address
* `-r` - Rate-limit per second
* `-b` - Forward address and domain (`example.com>127.0.0.1:3000`)

## Building
Development:
```
go run main.go
go build
```

Release:
```
git tag -a v0.3.0 -m "message"
git push origin v0.3.0
goreleaser release
```