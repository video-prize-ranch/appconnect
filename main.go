package main

import (
	"bufio"
	"flag"
	"log"
	"net/http"
	"net/url"
	"regexp"
	"strings"
	"time"

	"github.com/JGLTechnologies/gin-rate-limit"
	"github.com/gin-gonic/gin"
)

type arrayFlags []string

func (i *arrayFlags) String() string {
	return ""
}

func (i *arrayFlags) Set(value string) error {
  *i = append(*i, value)
  return nil
}

var Backends arrayFlags

var botRe1 = regexp.MustCompile(`(?i)(bot|Baiduspider|Yahoo! Slurp|Netvibes|zgrab|01h4x.com|360Spider|404(checker|enemy)|80legs|ADmantX|ALittle Client|ASPSeek|Abonti|Aboundex|Acunetix|AllSubmitter|Alligator|Anarchie|Anarchy|Anarchy99|Ankit|Anthill|Apexoo|Aspiegel|Asterias|Attach|Awario|BBBike|BDFetch|BackStreet|BackWeb|Backlink|Badass|Bandit|Barkrowler|BatchFTP|Battleztar Bazinga|Bigfoot|Bitacle|BlackWidow|Black Hole|Blackboard|Blow|BlowFish|Boardreader|Bolt|Brandprotect|Brandwatch|Buck|Buddy|BuiltWith|Bullseye|BunnySlippers|BuzzSumo|CATExplorador|CODE87|CSHttp|Calculon|Cegbfeieh|CensysInspect|CheTeam|CherryPicker|ChinaClaw|Chlooe|Citoid|Cloud mapping|Collector|Copier|CopyRight|Copyscape|Cosmos|Crawling at Home Project|CrazyWebCrawler|Crescent|Curious|Custo|CyotekWebCopy|DSearch|DTS Agent|DataCha0s|DatabaseDriverMysqli|Demon|Deusu|Devil|Digincore|DigitalPebble|Dirbuster|Disco|Dispatch|DittoSpyder|DnBCrawler-Analytics|Domain|Download Wonder|Dragonfly|Drip|ECCP/1.0|EMail Siphon|EMail Wolf|EasyDL|Ebingbong|Ecxi|EirGrabber|EroCrawler|Evil|Express WebPictures|Extractor|ExtractorPro|Extreme Picture Finder|EyeNetIE|Ezooms|FDM|FHscan|Fimap|Firefox/7.0|FlashGet|Flunky|Freeuploader|FrontPage|Fuzz|FyberSpider|G-i-g-a-b-o-t|GT::WWW|Genieo|GermCrawler|GetRight|GetWeb|Getintent|Go!Zilla|Go-Ahead-Got-It|GoZilla|Gotit|GrabNet|Grabber|Grafula|GrapeFX|GrapeshotCrawler|HEADMasterSEO|HMView|HTMLparser|HTTP::Lite|HTTrack|Haansoft|HaosouSpider|Harvest|Havij|Heritrix|Hloader|Humanlinks|IDBTE4M|Iblog|Id-search|Image Fetch|Image Sucker|Indy Library|InfoTekies|Intelliseek|InterGET|InternetSeer|Internet Ninja|Iria|Iskanie|JOC Web Spider|Jbrofuzz|JetCar|Jetty|JikeSpider|Joomla|Jorgee|JustView|Kenjin Spider|Keyword Density|Kinza|LNSpiderguy|LWP::Simple|Larbin|Leap|LeechFTP|LeechGet|Lftp|LibWeb|Libwhisker|LieBaoFast|Lightspeedsystems|Likse|LinkScan|LinkWalker|LinkextractorPro|LinksManager|Lipperhey|Litemage_walker|Lmspider|Ltx71|MFC_Tear_Sample|MIDown tool|MIIxpc|MQQBrowser|MSFrontPage|MSIECrawler|Mag-Net|Magnet|Majestic12|MarkMonitor|MarkWatch|Mass Downloader|Masscan|Mata Hari|Mb2345Browser|MegaIndex.ru|Metauri|MicroMessenger|Microsoft Data Access|Microsoft URL Control|Minefield|Mister PiX|Moblie Safari|Mojeek|Mojolicious|Morfeus Fucking Scanner|Mozlila|Mr.4x3|NICErsPRO|Name Intelligence|Nameprotect|Navroad|NearSite|Needle|Nessus|NetAnts|NetLyzer|NetMechanic|NetSpider|NetZIP|Net Vampire|Netcraft|Nettrack|Netvibes|Nibbler|Nikto|NimbleCrawler|Nimbostratus|Ninja|Nmap|Not|Nuclei|Nutch|Octopus|Offline Explorer|Offline Navigator|OnCrawl|OpenLinkProfiler|OpenVAS|Openfind|Openvas|OrangeSpider|PECL::HTTP|PHPCrawl|POE-Component-Client-HTTP|PageAnalyzer|PageGrabber|PageScorer|PageThing.com|Page Analyzer|Pandalytics|Panscient|Papa Foto|Pavuk|PeoplePal|Pi-Monster|Picscout|Picsearch|PictureFinder|Piepmatz|Pimonster|Pixray|PleaseCrawl|Pockey|ProWebWalker|Probethenet|Pu_iN|Pump|PxBroker|PyCurl|QueryN Metasearch|Quick-Crawler|RankActive|Re-re|ReGet|RealDownload|Reaper|RebelMouse|Recorder|RedesScrapy|RepoMonkey|Ripper|RocketCrawler|SBIder|SEO|SISTRIX|SalesIntelligent|ScanAlert|ScoutJet|Scrapy|Screaming|Searchestate|Seekport|SemanticJuice|Semrush|Shodan|Siphon|SiteExplorer|SiteLockSpider|SiteSnagger|SiteSucker|Site Sucker|Sitebeam|Siteimprove|Sitevigil|SlySearch|SmartDownload|Snake|Snoopy|Sociscraper|Sogou web spider|Sosospider|Sottopop|SpaceBison|Spammen|Spanner|Spinn3r|Sqlmap|Sqlworm|Sqworm|Steeler|Stripper|Sucker|Sucuri|SuperHTTP|Suzuran|Szukacz|T0PHackTeam|Teleport|Telesoft|Telesphoreo|Telesphorep|TheNomad|The Intraformant|Thumbor|Titan|Toata|Tracemyfile|Trendiction|Turingos|Turnitin|Twice|Typhoeus|URLy.Warning|URLy Warning|Upflow|VB Project|VCI|Vacuum|Vagabondo|VelenPublicWebCrawler|VeriCiteCrawler|VidibleScraper|Virusdie|VoidEYE|Voil|Voltron|WEBDAV|WPScan|WWW-Collector-E|WWW-Mechanize|WWW::Mechanize|WWWOFFLE|Wallpapers|Wallpapers/3.0|WallpapersHD|WeSEE|WebAuto|WebBandit|WebCollage|WebCopier|WebEnhancer|WebFetch|WebFuck|WebGo IS|WebImageCollector|WebLeacher|WebPix|WebReaper|WebSauger|WebStripper|WebSucker|WebWhacker|WebZIP|Web Auto|Web Collage|Web Enhancer|Web Fetch|Web Fuck|Web Pix|Web Sauger|Web Sucker|Webalta|Webshag|WebsiteExtractor|WebsiteQuester|Website Quester|Webster|Whack|Whacker|Whatweb|Widow|WinHTTrack|Wotbox|Wprecon|Xaldon WebSpider|Xaldon_WebSpider|Xenu|Zade|Zauba|Zermelo|Zeus|ZmEu|ZyBorg|arquivo-web-crawler|arquivo.pt|autoemailspider|backlink-check|cah.io.community|check1.exe|clark-crawler|com.plumanalytics|crawl.sogou.com|crawler.feedback|crawler4j|dataforseo.com|domainsproject.org|eCatch|evc-batch|facebookscraper|gopher|heritrix|instabid|internetVista monitor|ips-agent|isitwp.com|iubenda-radar|lwp-request|lwp-trivial|magpie-crawler|mediawords|muhstik-scan|netEstate NE Crawler|page scorer|pcBrowser|plumanalytics|polaris version|probe-image-size|ripz|s1z.ru|satoristudio.net|scalaj-http|scan.lol|sexsearcher|sitechecker.pro|siteripz|sogouspider|spyfu|sysscan|tAkeOut|trendiction.com|trendiction.de|ubermetrics-technologies.com|voyagerx.com|webgains|webmeup-crawler|webpros|x09Mozilla|x22Mozilla|xpymep1.exe|zauba.io|zgrab)`)
var botRe2 = regexp.MustCompile(`(?i)(curl|wget|scrapy|splash|javafx|python(-requests)?|java|jakarta|(syn)?(go-)?(ok)?http-?client|jersey|libwww-perl|ruby)`)

func keyFunc(c *gin.Context) string {
	return c.GetHeader("Fly-Client-IP")
}

func rlErrorHandler(c *gin.Context, info ratelimit.Info) {
	c.String(429, "Too many requests")
}

func main() {
	gin.SetMode(gin.ReleaseMode)
	r := gin.Default()

	addr := flag.String("l", ":5000", "Address to listen on")
	limit := flag.Int("r", 5, "Ratelimit limit per second")
	flag.Var(&Backends, "b", "Upstream server address and domain. Example: example1.com>127.0.0.1:3000")
	flag.Parse()

	store := ratelimit.InMemoryStore(&ratelimit.InMemoryOptions{
		Rate:  time.Second,
		Limit: uint(*limit),
	})
	mw := ratelimit.RateLimiter(store, &ratelimit.Options{
		ErrorHandler: rlErrorHandler,
		KeyFunc: keyFunc,
	})

	backends := map[string]string{}
	for _, backend := range Backends {
		be := strings.Split(backend, ">")
		backends[be[0]] = be[1]
	}

	r.Any("/*path", mw, func(c *gin.Context) {
		if strings.HasPrefix(c.Request.URL.Path, "/health") || c.Request.UserAgent() == "InstanceTools-Uptime" {
			c.String(http.StatusOK, "")
			return
		}

		if strings.HasSuffix(c.Request.URL.Path, ".php") || strings.HasSuffix(c.Request.URL.Path, ".aspx") {
			c.Redirect(301, "https://speed.hetzner.de/10GB.bin")
		}

		if !(strings.HasSuffix(c.Request.URL.Path, "/rss") || strings.HasPrefix(c.Request.URL.Path, "/api") || c.Request.Host == "ci.bcow.xyz") {
			if botRe1.MatchString(c.Request.UserAgent()) || botRe2.MatchString(c.Request.UserAgent()) {
				c.String(http.StatusForbidden, "")
				return
			}

			if c.Request.Header.Get("Accept-Language") == "" {
				c.String(http.StatusForbidden, "")
				return
			}
		}

		if c.Request.UserAgent() == "" && c.Request.Host != "ci.bcow.xyz" {
			c.String(http.StatusForbidden, "")
			return
		}

		if strings.HasPrefix(c.Request.Header.Get("X-Forwarded-For"), "2a01:4f") {
			c.String(http.StatusForbidden, "")
			return
		}

		proxy, err := url.Parse("http://" + backends[c.Request.Host])
		if err != nil {
			log.Printf("error in parse addr: %v", err)
			c.String(500, "error")
			return
		}
		req := c.Request
		req.URL.Scheme = proxy.Scheme
		req.URL.Host = proxy.Host

		transport := http.DefaultTransport
		resp, err := transport.RoundTrip(req)
		if err != nil {
			log.Printf("error in roundtrip: %v", err)
			c.String(500, "error")
			return
		}

		for k, vv := range resp.Header {
			for _, v := range vv {
				c.Header(k, v)
			}
		}

		defer resp.Body.Close()
		bufio.NewReader(resp.Body).WriteTo(c.Writer)
	})

	if err := r.Run(*addr); err != nil {
		log.Printf("Error: %v", err)
	}
}
